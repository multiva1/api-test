package com.api.test.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.api.test.entity.Test;
import com.api.test.repository.TestRepository;

/**
 * 
 * @author Enrique
 *
 */
@Service
public class TestService {

	@Autowired
	private TestRepository testRepository;
	
	/**
	 * 
	 * @param model
	 * @return
	 */
	public Test save(Test entity) {
		return testRepository.save(entity);
	}
	
	/**
	 * 
	 * @param entities
	 * @return
	 */
	public List<Test>  saveAll(Iterable<Test> entities) {
		return testRepository.saveAll(entities);
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Test> findAll() {
		return testRepository.findAll();
	}
}
