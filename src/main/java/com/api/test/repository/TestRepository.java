package com.api.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.api.test.entity.Test;

/**
 * 
 * @author Enrique
 *
 */
@Repository
public interface TestRepository extends JpaRepository<Test, Integer>{

}
