package com.api.test.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 * 
 * @author Enrique
 *
 */
@Data
@Entity
@Table(name = "test")
public class Test implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 5686884474605589454L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "fechaVenta")
	private Integer fechaVenta;
	private BigDecimal monto;
	private Integer sucursal;
	private String cliente;
	private String canal;
	
	
}
