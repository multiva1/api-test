package com.api.test.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.api.test.entity.Test;
import lombok.Data;

/**
 * 
 * @author Enrique
 *
 */
@Data
public class TestModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5083823927403310411L;

	private Integer id;
	@NotNull(message = "fechaVenta is mandatory")
	private Integer fechaVenta;
	@NotNull(message = "monto is mandatory")
	private BigDecimal monto;
	@NotNull(message = "sucursal is mandatory")
	private Integer sucursal;
	@NotBlank(message = "cliente is mandatory")
	private String cliente;
	@NotBlank(message = "canal is mandatory")
	private String canal;
	
	public TestModel(Integer id,Integer fechaVenta,BigDecimal monto,Integer sucursal,String cliente,String canal) {
		this.id=id;
		this.fechaVenta=fechaVenta;
		this.monto=monto;
		this.sucursal=sucursal;
		this.cliente=cliente;
		this.canal=canal;
		
	}
	
	public TestModel(Test test) {
		this.id=test.getId();
		this.fechaVenta=test.getFechaVenta();
		this.monto=test.getMonto();
		this.sucursal=test.getSucursal();
		this.cliente=test.getCliente();
		this.canal=test.getCanal();
	}
	
	public TestModel() {
		
	}
}
