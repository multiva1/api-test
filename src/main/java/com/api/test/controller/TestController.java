package com.api.test.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.test.entity.Test;
import com.api.test.model.TestModel;
import com.api.test.service.TestService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.media.ArraySchema;
/**
 * 
 * @author Enrique
 *
 */
@RestController
@RequestMapping("/test")
@Tag(name = "test", description = "Test API with documentation")
public class TestController {

	@Autowired
	private TestService testService;
	
	/**
	 * 
	 * @param model
	 * @return
	 */
//	@Operation(summary = "Save test")
//	@ApiResponses(value = {
//            @ApiResponse(responseCode = "201", description = "test created", content = { @
//                    Content(mediaType = "application/json", schema = @Schema(implementation = TestModel.class))}),
//            @ApiResponse(responseCode = "404", description = "Bad request", content = @Content) })
//	@PostMapping
//	public ResponseEntity<TestModel> create(@Parameter(description = "test object to be created") @Valid @RequestBody TestModel model) {
//		
//		
//		if(model==null)
//			 return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//		
//		Test entity= new Test();
//		BeanUtils.copyProperties(model, entity);
//		
//		return ResponseEntity.status(HttpStatus.CREATED).body( new TestModel(testService.save(entity)));
//	}
	
	/**
	 * 
	 * @param model
	 * @return
	 */
	@Operation(summary = "Save list test")
	@ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "test created", content = { @
                    Content(mediaType = "application/json", schema = @Schema(implementation = TestModel.class))}),
            @ApiResponse(responseCode = "404", description = "Bad request", content = @Content) })
	@PostMapping
	public ResponseEntity<List<Test>> createList(@Parameter(description = "list test object to be created") @RequestBody List<Test> model) {
		
		
		if(model.isEmpty())
			 return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

		return ResponseEntity.status(HttpStatus.CREATED).body(testService.saveAll(model));
	}
	
	/**
	 * 
	 * @return
	 */
	@Operation(summary = "Get all tests")
    @ApiResponses(value = { 
            @ApiResponse(responseCode = "200", description = "found test", content = { 
                @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = TestModel.class)))}), 
            @ApiResponse(responseCode = "404", description = "No Test found", content = @Content) })
    @GetMapping
    public ResponseEntity<List<TestModel>> getAll() {
		
        List<Test> list = (List<Test>) testService.findAll();
        
        if (list.isEmpty()) 
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        
        return ResponseEntity.status(HttpStatus.OK).body(list.stream().map(item -> new TestModel(item)).collect(Collectors.toList()));
    }
}
